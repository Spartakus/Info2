package de.hska.iwii.i2.dliste;

class DGListe <E> {

    /**
     * Counts amount of Elements
     */
    private int size;

    /**
     * Points to first Element
     */
    private ListElement first;

    /**
     * Points to last Element
     */
    private ListElement last;

    /**
     * Points to current element
     */
    private ListElement current;

    /**
     * F�gt einen String zum Anfang der Liste hinzu
     * @param value  Content
     */
    public void  addFirst(E value)
    {
        ListElement newElement = new ListElement( value );

        if ( this.first != null ) {
            this.first.setPrevious(newElement);
            newElement.setNext(this.first);
        } else {
            this.last = newElement;
        }

        this.first = newElement;
        this.size++;
    }

    /**
     * Append a Stringelement
     * @param value Content
     */
    public void addLast(E value)
    {
        ListElement newElement = new ListElement( value );

        if ( this.last != null ) {
            this.last.setNext(newElement);
            newElement.setPrevious(this.last);
        } else {
            this.first = newElement;
        }

        this.last = newElement;
        this.size++;
    }

    /**
     * Adding Listelement to given position
     * @param Index Index of position
     * @param value String to add
     */
    public void add(int Index, E value)
    {
        if (Index <= 0)

            this.addFirst( value );

        else if (Index >= this.size - 1)

            this.addLast( value );

        else {

            ListElement current = this.getElement( Index );
            if (current != null){

                ListElement newElement = new ListElement(value);

                /* Linking current's predecessor and current with New Element */
                ListElement currentPredecessor = current.getPrevious();
                currentPredecessor.setNext(newElement);

                current.setPrevious(newElement);

                newElement.setPrevious( currentPredecessor );
                newElement.setNext(current);
                this.size++;
            }

        }


    }

    /**
     * Getting String at a certain position
     * @param Index Index of Position
     * @return String of Position
     */
    public E get(int Index){
        if ( this.size == 0 )
            this.current = null;
        else if ( Index <= 0 )
            this.current = this.first;
        else if ( Index >= this.size - 1 )
            this.current = this.last;
        else
            this.current = this.getElement(Index);

        return ( this.current == null ? null : this.current.getContent() );
    }

    /**
     * Getting String at a certain position
     * @param Index Index of Position
     * @return String of Position
     */
    private ListElement getElement(int Index){

        assert Index > 0 && Index < this.size;

        ListElement currentElement;

        // Getting most efficient way by checking distance
        int halfSize = this.size / 2;

        if ( Index < halfSize )
        {
            // Start from First
            currentElement = this.first;
            for ( int cur = 0; cur < Index; cur++)
            {
                currentElement = currentElement.getNext();
            }

        } else {
            // Start from Last
            currentElement = this.last;
            for ( int cur = this.size - 1; cur > Index; cur-- )
            {
                currentElement = currentElement.getPrevious();
            }

        }

        return currentElement;
    }

    /**
     * Deleting the last Element of List
     * @return Deleted Element
     */
    public E removeFirst(){

        if (this.first == null) return null;

        ListElement oldFirst = this.first;

        this.first = oldFirst.getNext();

        if (this.first != null)
            this.first.setPrevious(null);

        this.size--;
        return oldFirst.getContent();
    }

    /**
     * Deleting the First Element of List
     * @return Deleted Element
     */
    public E removeLast(){

        if (this.last == null) return null;

        ListElement oldLast = this.last;

        this.last = oldLast.getPrevious(); //Worst Case = it's null

        if (this.last != null)
            this.last.setNext(null);

        this.size--;
        return oldLast.getContent();
    }

    /**
     * Returning List Size
     * @return ...
     */
    public int getSize(){
        return this.size;
    }

    public E getNext(){
        if (this.current != null)
            this.current = this.current.getNext();

        E content = null;
        if (this.current != null)
            content = this.current.getContent();

        return content;
    }

    /**
     * Returns previous item
     * @return previous item or NULL if not existing
     */
    public E getPrevious(){
        if (this.current != null)
            this.current = this.current.getPrevious();

        E content = null;

        if (this.current != null)
            content = this.current.getContent();

        return content;
    }

    /**
     * Returns the first String in the list
     * @return First String in List
     */
    public E getFirst() {
        this.current = this.first;

        if (this.current != null)
            return this.current.getContent();
        return null;
    }

    public E getLast() {
        this.current = this.last;

        if (this.current != null)
            return this.current.getContent();
        return null;
    }

    class ListElement{

        /**
         * Saves next Element
         */
        ListElement next = null;

        /**
         * Saves previous Element
         */
        ListElement previous = null;

        /**
         * Saves Content of current element
         */
        E content;

        /**
         * CONSTRUCTOR - Sets initial Strng
         * @param content Strngcontent of current Element
         */
        public ListElement(E content) {
            this.content = content;
        }

        /**
         * Gets next Element
         * @return next Next Element
         */
        public ListElement getNext() {
            return next;
        }

        /**
         * Sets Next Element
         * @param next Next Element
         */
        public void setNext(ListElement next) {
            this.next = next;
        }

        /**
         * Gets previous Element
         * @return Previous Element
         */
        public ListElement getPrevious() {
            return previous;
        }

        /**
         * Sets previous Element
         * @param previous Previous Element
         */
        public void setPrevious(ListElement previous) {
            this.previous = previous;
        }

        /**
         * Gets content of current ListElement
         * @return Stringcontent
         */
        public E getContent() {
            return content;
        }

        /**
         * Sets Content of current ListElement
         * @param content String to be saved
         */
        public void setContent(E content) {
            this.content = content;
        }

        public String toString(){
            return this.getContent().toString();
        }

    }
}