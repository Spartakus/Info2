package de.hska.iwii.i2.dliste;

import de.hska.iwii.i2.dliste.DListe;

class Main {

    protected static Main application;

    public static void main(String[] args) {
        application = new Main();
        application.run();
    }


    private DListe list;
    public Main(){
        this.list = new DListe();
    }

    public void run(){
        this.list.addFirst("test1");
        this.list.addFirst("test2");
        this.list.addFirst("test3");
        this.list.addFirst("test4");
        this.list.addFirst("test5");
        this.list.addLast("test6");
        this.list.addLast("test7");
        this.list.addLast("test8");
        this.list.addLast("test9");
        this.list.addLast("test10");
        this.list.addLast("test11");

        for ( int i = 0; i < this.list.getSize(); i++ )
            System.out.println(this.list.get(i));


        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        String current = this.list.getFirst();
        do {
            System.out.println(current);
        } while( (current = this.list.getNext()) != null );


        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        for ( int i = 0; i < this.list.getSize(); i++ )
            System.out.println(this.list.get(i));


    }

}