package de.hska.iwii.i2.dliste;

import junit.framework.TestCase;

/**
 * double list test
 * Created by domi on 03-Nov-15.
 */
public class DListeTest extends TestCase {

    DGListe<String> testList;

    public void setUp() throws Exception {
        super.setUp();

        this.testList = new DGListe<>();

        this.testList.addLast("Erstes");
        this.testList.addLast("Zweites");

    }

    public void tearDown() throws Exception {

    }

    public void testAddFirst() throws Exception {

        this.testList.addFirst("Ich bin der Erste");

        if ( !this.testList.removeFirst().equals("Ich bin der Erste") )
        {
            throw new Exception("AddFirst failed");
        }

    }

    public void testAddLast() throws Exception {

        this.testList.addLast("Ich bin der Letzte");

        if ( !this.testList.removeLast().equals("Ich bin der Letzte") )
        {
            throw new Exception("AddLast failed");
        }
    }

    public void testAdd() throws Exception {


        this.testList.addLast("Vorvorletzter");
        this.testList.addLast("Vorletzter");
        this.testList.addLast("Letzter");

        this.testList.add(1, "Ich bin der Zweite");

        if ( !this.testList.get(1).equals("Ich bin der Zweite") )
        {
            throw new Exception("Add failed");
        }

    }

    public void testGet() throws Exception {
        this.testList.addFirst("Ich bin der erste Eintrag");

        if ( !this.testList.getFirst().equals("Ich bin der erste Eintrag") ){
            throw new Exception("Get failed");
        }
    }

    public void testRemoveFirst() throws Exception {
        this.testList.addFirst("Der Erste String");

        if (!this.testList.removeFirst().equals("Der Erste String"))
            throw new Exception("RemoveFirst failed");
    }

    public void testRemoveLast() throws Exception {
        this.testList.addLast("Der Letzte String");

        if (!this.testList.removeLast().equals("Der Letzte String"))
            throw new Exception("RemoveLast failed");
    }

    public void testGetSize() throws Exception {
        DListe testList = new DListe();
        testList.addFirst("Ich bin ein Eintrag");
        testList.addLast("Ich bin ein weiterer Eintrag");

        if (testList.getSize() != 2)
            throw new Exception("GetSize failed");

    }

    public void testGetNext() throws Exception {
        this.testList.addFirst("Zweiter Eintrag");
        this.testList.addFirst("Erster Eintrag");

        String temp = this.testList.getFirst();

        if ( !this.testList.getNext().equals("Zweiter Eintrag") )
            throw new Exception("GetNext failed");

    }

    public void testGetPrevious() throws Exception {
        this.testList.addLast("Vorletzter Eintrag");
        this.testList.addLast("Letzter Eintrags");

        String temp = this.testList.getLast();

        if ( !this.testList.getPrevious().equals("Vorletzter Eintrag") )
            throw new Exception("GetPrevious failed");
    }

    public void testGetFirst() throws Exception {
        this.testList.addFirst("Erster Eintrag");

        if (!this.testList.getFirst().equals("Erster Eintrag"))
            throw new Exception("GetFirst failed");
    }

    public void testGetLast() throws Exception {
        this.testList.addLast("Letzter Eintrag");

        if (!this.testList.getLast().equals("Letzter Eintrag"))
            throw new Exception("GetLast failed");
    }
}