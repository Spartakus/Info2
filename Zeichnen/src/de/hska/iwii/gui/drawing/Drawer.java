package de.hska.iwii.gui.drawing;

import com.sun.javafx.geom.*;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.scene.shape.Shape;

import java.util.Vector;

/**
 * Whatever.. #unused
 * Created by domi on 23.10.14.
 */
public class Drawer extends Pane {

    final static double preferedWidth = 1000;
    final static double preferedHeight = 800;


    public Drawer() {
        super();

        this.setPrefSize( Drawer.preferedWidth, Drawer.preferedHeight );
        //this.testDraw();
    }

    private void testDraw(){

        Vector<Node> shapes = new Vector<Node>();

        for ( int i = 0; i < 5 ; i++ )
        {
            Shape temp = new Circle( (14 + 40) * (i+1) , (14 + 40) * (i+1) , 20);
            temp.setStroke( new Color(0.75,0.15,0.8,1) );
            temp.setStrokeWidth(2);

            Paint color = new Color(.1, .25,.5,.75);
            temp.setFill( color );

            shapes.add( temp );
        }
        this.getChildren().addAll( shapes );
    }
}
