package de.hska.iwii.gui.drawing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

import javafx.scene.paint.Color;

public class DrawGroup implements DrawObject {

	/**
	 * Savers all Groupmembers
	 */
	HashSet<DrawObject> members;
	double clickedX, clickedY;
	boolean isHightlighted;
	
	@Override
	public void clicked(double posX, double posY) {
		this.clickedX = posX;
		this.clickedY = posY;
	}
	
	@Override
	public void highlight() {
		this.isHightlighted = true;
		for( DrawObject member: members )
			member.highlight();
	}

	@Override
	public void unhighlight() {
		this.isHightlighted = false;
		for( DrawObject member: members )
			member.unhighlight();
	}

	@Override
	public void move(double posX, double posY) {
		// Sp�ter
	}

	@Override
	public double getStartX() {
		Double startX = null;
		
		for( DrawObject member: members ){
			if ( member.getStartX() < startX.doubleValue() )
				startX = member.getStartX();
		}
		
		return startX.doubleValue();
	}

	@Override
	public double getStartY() {
		Double startY = null;
		
		for( DrawObject member: members ){
			if ( member.getStartY() < startY.doubleValue() )
				startY = member.getStartY();
		}
		
		return startY.doubleValue();
	}

	@Override
	public double getEndX() {
		Double endX = null;
		
		for( DrawObject member: members ){
			if ( member.getEndX() > endX.doubleValue() )
				endX = member.getEndX();
		}
		
		return endX.doubleValue();
	}

	@Override
	public double getEndY() {
		Double endY = null;
		
		for( DrawObject member: members ){
			if ( member.getEndY() > endY.doubleValue() )
				endY = member.getEndY();
		}
		
		return endY.doubleValue();
	}

	@Override
	public double getMiddleX() {
		double startX, endX;
		
		startX = this.getStartX();
		endX = this.getEndX();
		
		return Math.abs( endX - startX );
	}

	@Override
	public double getMiddleY() {
		double startY, endY;
		
		startY = this.getStartY();
		endY = this.getEndY();
		
		return Math.abs( endY - startY );
	}

	@Override
	public void changeColor(Color color) {
		for( DrawObject member: members ){
			member.changeColor(color);
		}
	}

	@Override
	public void rotate(double angle) {
		for( DrawObject member: members ){
			// ?!
		}
	}

	@Override
	public void translate(double x, double y) {
		// TODO Auto-generated method stub

	}

	@Override
	public void zoom(double factor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void copy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub

	}

	@Override
	public void paste() {
		// TODO Auto-generated method stub

	}

	@Override
	public void cut() {
		// TODO Auto-generated method stub

	}

}
