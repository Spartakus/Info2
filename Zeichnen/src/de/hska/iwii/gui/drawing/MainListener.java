package de.hska.iwii.gui.drawing;

import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.*;

/**
 * Listener for drawing events
 * Created by domi on 24.10.14.
 */
public class MainListener implements DrawingListener{

    private Drawer mainPane;
    private String currentType;
    private Shape currentFigure;

    private double currentX;
    private double currentY;

    private double diffX;
    private double diffY;

    private Hashtable<Integer,Node> selected;

    public MainListener(Drawer panel)
    {
        this.mainPane = panel;
        this.selected = new Hashtable<Integer, Node>();
    }

    @Override
    public void startCreateFigure(String figureType, double xPos, double yPos) {
        System.out.println("Start Creating Figure");

        Shape newFigure;

        switch ( figureType )
        {
            case "ellipse":
                newFigure = new Ellipse(xPos, yPos, 1, 1);
                newFigure.setFill( new Color(0.25, 0.25, 0.5, 1) );
                break;
            case "rectangle":
                newFigure = new Rectangle(xPos, yPos, 1, 1);
                newFigure.setFill( new Color(0.25, 0.5, 0.25, 1) );
                break;
            default:
            case "line":
                newFigure = new Line(xPos, yPos, xPos + 1, yPos + 1);
                newFigure.setFill( new Color(0.5, 0.25, 0.25, 1) );
                newFigure.setStrokeWidth(4);
                break;
        }

        this.currentFigure = newFigure;
        this.currentType = figureType;
        this.currentX = xPos;
        this.currentY = yPos;
        this.mainPane.getChildren().add( newFigure );
    }

    @Override
    public void startMoveFigure(Node node, double xPos, double yPos) {

        if ( node instanceof Ellipse ) {
            Ellipse el = (Ellipse) node;

            this.currentX = el.getCenterX();
            this.currentY = el.getCenterY();

            this.diffX = xPos - this.currentX;
            this.diffY = yPos - this.currentY;

        } else if ( node instanceof Rectangle ){
            Rectangle rec = (Rectangle) node;

            this.currentX = rec.getX();
            this.currentY = rec.getY();

            this.diffX = xPos - this.currentX;
            this.diffY = yPos - this.currentY;

        } else if ( node instanceof Line ){
            Line lin = (Line) node;

            this.currentX = lin.getStartX();
            this.currentY = lin.getStartY();

            this.diffX = xPos - this.currentX;
            this.diffY = yPos - this.currentY;
        }
    }

    @Override
    public void workCreateFigure(double xPos, double yPos) {
        double width, height;


        switch( this.currentType )
        {
            case "ellipse":

                double centerX, centerY, radiusX, radiusY;

                centerX = ( xPos + this.currentX ) / 2;
                centerY = ( yPos + this.currentY ) / 2;

                radiusX = Math.abs(xPos - centerX);

                radiusY = Math.abs(centerY - yPos);

                Ellipse cur = (Ellipse)this.currentFigure;

                cur.setCenterX( centerX );
                cur.setCenterY( centerY );
                cur.setRadiusX(radiusX);
                cur.setRadiusY(radiusY);
                break;

            case "rectangle":
                Rectangle curRec = (Rectangle) this.currentFigure;

                width = Math.abs( this.currentX - xPos );

                if ( xPos < this.currentX )
                {
                    curRec.setX( xPos );
                } else {
                    curRec.setX( this.currentX );
                }

                height = Math.abs( this.currentY - yPos );
                if ( yPos < this.currentY )
                {
                    curRec.setY( yPos );
                } else {
                    curRec.setY( this.currentY );
                }

                curRec.setWidth( width );
                curRec.setHeight( height );
                break;

            default:
            case "line":
                Line curLi = (Line)this.currentFigure;
                curLi.setEndX( xPos );
                curLi.setEndY( yPos );
                break;

        }

    }

    @Override
    public void workMoveFigure(Node node, double xPos, double yPos) {

        if ( node instanceof Ellipse ){

            Ellipse el = (Ellipse) node;
            el.setCenterX( xPos - this.diffX );
            el.setCenterY( yPos - this.diffY );

        } else if ( node instanceof Rectangle ) {

            Rectangle rec = (Rectangle) node;
            rec.setX( xPos - this.diffX );
            rec.setY( yPos - this.diffY );

        } else if ( node instanceof Line ){

            Line lin = (Line) node;
            double diffEndStartX = lin.getEndX() - lin.getStartX();
            double diffEndStartY = lin.getEndY() - lin.getStartY();

            double newStartX = xPos - this.diffX;
            double newStartY = yPos - this.diffY;

            lin.setStartX( newStartX );
            lin.setStartY( newStartY );

            lin.setEndX( newStartX + diffEndStartX );
            lin.setEndY( newStartY + diffEndStartY );

        }
    }

    private void move( Ellipse ellipse, double xPos, double yPos )
    {
        ellipse.setCenterY(yPos);
        ellipse.setCenterX(xPos);
    }

    private void move( Rectangle rectangle, double xPos, double yPos )
    {
        rectangle.setY(yPos);
        rectangle.setX(xPos);
    }

    @Override
    public void endCreateFigure(double xPos, double yPos) {
        System.out.println("End Creating Figure");
    }

    @Override
    public void endMoveFigure(Node node, double xPos, double yPos) {
        System.out.println("End Moving Figure");
    }

    @Override
    public void selectFigure(Node node, double xPos, double yPos, boolean shiftPressed) {
        this.selected.replace(node.hashCode(), node);
        this.highlightSelected();
        System.out.println("Select Figure");

    }

    private void highlightSelected(){
        System.out.println("Highlighting");

        Color selected_color = new Color( 1, 1, .25, 1 );

        Enumeration nodes = this.selected.elements();
        Node current;
        while ( nodes.hasMoreElements() )
        {
            current = (Node) nodes.nextElement();
            if ( current instanceof Ellipse ){
                Ellipse el = (Ellipse) current;
                el.setStrokeWidth(2);
                el.setStroke( selected_color );
            } else if ( current instanceof Rectangle ) {
                Rectangle re = (Rectangle) current;
                re.setStrokeWidth(2);
                re.setStroke( selected_color );
            } else if ( current instanceof Line ) {
                Line li = (Line) current;
                currentFigure.setStroke( selected_color );
            }

        }
    }

    @Override
    public void rotate(Node node, double angle) {
        System.out.println("Rotate Figure");
    }

    @Override
    public void translate(Node node, double deltaX, double deltaY) {
        System.out.println("Translate Figure");
    }

    @Override
    public void zoom(Node node, double zoomFactor) {
        System.out.println("Zoom Figure");
    }

    @Override
    public void deleteFigures() {
        System.out.println("Delete Figures");
    }

    @Override
    public void copyFigures() {
        System.out.println("Copy Figures");
    }

    @Override
    public void pasteFigures() {
        System.out.println("Paste Figures");
    }

    @Override
    public void moveSelectedFiguresToTop() {
        System.out.println("Move Selected Figures to Top");
    }

    @Override
    public void moveSelectedFiguresToBottom() {
        System.out.println("Move selected Figures to Bottom");
    }

    @Override
    public void moveSelectedFiguresDown() {
        System.out.println("Move Selected Figures one Layer down");
    }

    @Override
    public void moveSelectedFiguresUp() {
        System.out.println("Move selected Figures one layer up");
    }

    @Override
    public void groupFigures() {
        System.out.println("Group selected Figures");
    }

    @Override
    public void ungroupFigures() {
        System.out.println("ungroup selected Figures");
    }

    @Override
    public int getSelectedFiguresCount() {
        return 0;
    }

    @Override
    public int getFiguresInClipboardCount() {
        return 0;
    }

    @Override
    public boolean isGroupSelected() {
        return false;
    }
}
