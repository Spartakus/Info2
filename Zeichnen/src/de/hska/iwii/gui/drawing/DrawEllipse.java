package de.hska.iwii.gui.drawing;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;

public class DrawEllipse extends Ellipse implements DrawObject {

	
	/**
	 * Some variables for additional stuff
	 */
	boolean isHightlighted;
	double clickedX, clickedY;
	double diffMiddleX, diffMiddleY; //Diff of clicked pos and actual object pos
	
	
	Color color;
	double angle = 0;
	double zoomFactor = 1;
	
	
	/**
	 * Constructor for DrawEllipse (Passing to super)
	 */
	public DrawEllipse(  ){ super(); }
	public DrawEllipse( double radiusX, double radiusY ){ super(radiusX, radiusY); }
	public DrawEllipse( double centerX, double centerY, double radiusX, double radiusY ){
		super( centerX, centerY, radiusX, radiusY);
	}
	
	@Override
	public void clicked(double posX, double posY) {
		this.clickedX = posX;
		this.clickedY = posY;
		
		this.diffMiddleX = Math.abs(getMiddleX() - posX);
		this.diffMiddleY = Math.abs(getMiddleY() - posY);
		
		this.highlight();
		
	}

	@Override
	public void highlight() {
		this.isHightlighted = true;
		this.setFill(new Color(255,0,0,1));
	}
	@Override
	public void unhighlight() {
		this.isHightlighted = false;
		this.setFill(this.color);
		
	}
	@Override
	public void move(double posX, double posY) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getStartX() {
		return this.getBoundsInParent().getMinX();
	}

	@Override
	public double getStartY() {
		return this.getBoundsInParent().getMinY();
	}

	@Override
	public double getEndX() {
		return this.getBoundsInParent().getMaxX();
	}

	@Override
	public double getEndY() {
		return this.getBoundsInParent().getMaxY();
	}

	@Override
	public double getMiddleX() {
		return this.getCenterX();
	}

	@Override
	public double getMiddleY() {
		return this.getCenterY();
	}

	@Override
	public void changeColor(Color color) {
		this.color = color;
		this.setFill(color);
	}

	@Override
	public void rotate(double angle) {
		this.setRotate(angle);
	}

	@Override
	public void translate(double x, double y) {
		this.setTranslateX(x);
		this.setTranslateY(y);
	}

	@Override
	public void zoom(double factor) {
		this.setScaleX(factor);
		this.setScaleY(factor);
	}

	@Override
	public void copy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub

	}

	@Override
	public void paste() {
		// TODO Auto-generated method stub

	}

	@Override
	public void cut() {
		// TODO Auto-generated method stub

	}

}
