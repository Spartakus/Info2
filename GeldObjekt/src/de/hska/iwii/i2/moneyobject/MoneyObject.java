package de.hska.iwii.i2.moneyobject;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * money object
 * Created by domi on 05-Nov-15.
 */
public class MoneyObject {
    private Currency currency;
    private double value;

    public MoneyObject( double value, Currency currency ){
        this.value = value;
        this.currency = currency;
    }

    public double getValue() {
        return this.value;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    public MoneyObject add( MoneyObject money ) throws Exception {
        if ( money.getCurrency().equals(this.currency) )
        {
            return new MoneyObject( this.value + money.getValue(), this.currency );
        }
        throw new Exception("W�hrungen stimmen nicht �berein!");
    }

    public MoneyObject substract( MoneyObject money ) throws Exception {
        if ( money.getCurrency().equals(this.currency) )
        {
            return new MoneyObject( this.value - money.getValue(), this.currency );
        }
        throw new Exception("W�hrungen stimmen nicht �berein!");
    }

    public String toString(){
        NumberFormat nf = NumberFormat.getNumberInstance( Locale.GERMAN );
        DecimalFormat df = (DecimalFormat)nf;
        String formatedMoney = df.format( this.value );

        return "EUR " + formatedMoney;
    }
}
