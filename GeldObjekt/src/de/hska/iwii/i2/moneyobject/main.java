package de.hska.iwii.i2.moneyobject;

import de.hska.iwii.i2.moneyobject.MoneyObject;

import java.util.Currency;

class Main {

    protected static Main application;

    public static void main(String[] args) {
        application = new Main();
        application.run();
    }

    public void run(){
        try {
            MoneyObject schein1 = new MoneyObject(100.25, Currency.getInstance("EUR"));
            MoneyObject schein2 = new MoneyObject(100.50, Currency.getInstance("EUR"));
            MoneyObject schein3 = new MoneyObject(100, Currency.getInstance("EUR"));

            MoneyObject kontostand = schein1.add(schein2).add(schein3);
            System.out.println("Der neue Kontostand betraegt " + kontostand);
        }
        catch (Exception e)
        {
            System.out.println("Es gab ein Problem:");
            System.out.println( e.toString() );
        }

    }

}