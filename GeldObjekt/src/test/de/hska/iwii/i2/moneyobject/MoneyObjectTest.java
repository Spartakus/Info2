package test.de.hska.iwii.i2.moneyobject; 

import de.hska.iwii.i2.moneyobject.MoneyObject;
import junit.framework.Test;
import junit.framework.TestSuite; 
import junit.framework.TestCase;

import java.util.Currency;

/** 
* MoneyObject Tester. 
* 
* @author <d_wuest>
* @since <pre>11/05/2015</pre> 
* @version 1.0 
*/ 
public class MoneyObjectTest extends TestCase { 
public MoneyObjectTest(String name) { 
super(name); 
} 

public void setUp() throws Exception { 
super.setUp(); 
} 

public void tearDown() throws Exception { 
super.tearDown(); 
} 

/** 
* 
* Method: getValue() 
* 
*/ 
public void testGetValue() throws Exception {
    MoneyObject myMoney = new MoneyObject( 400.35, Currency.getInstance("EUR")  );

    if ( myMoney.getValue() != 400.35 )
        throw new Exception("getValue failed");
} 

/** 
* 
* Method: getCurrency() 
* 
*/ 
public void testGetCurrency() throws Exception {
    MoneyObject myMoney = new MoneyObject( 1.4, Currency.getInstance("EUR")  );

    if ( myMoney.getCurrency() != Currency.getInstance("EUR") )
        throw new Exception("getCurrency failed");
} 

/** 
* 
* Method: add(MoneyObject money) 
* 
*/ 
public void testAdd() throws Exception {
    MoneyObject myMoney = new MoneyObject( 9.5, Currency.getInstance("EUR")  );
    MoneyObject myMoney2 = new MoneyObject( 0.5, Currency.getInstance("EUR")  );
    MoneyObject myMoney3 = new MoneyObject( 15, Currency.getInstance("USD")  );

    if ( myMoney.add(myMoney2).getValue() != 10 )
        throw new Exception("testAdd failed - Add is wrong");

    try {
        MoneyObject tempObject = myMoney.add(myMoney3); // Should fail

        throw new Exception("testAdd failed - Currency check does not work!");
    } catch (Exception e){
        // Do nothing,
        // Exception is expected
    }

} 

/** 
* 
* Method: substract(MoneyObject money) 
* 
*/ 
public void testSubstract() throws Exception {
    MoneyObject myMoney = new MoneyObject( 9.5, Currency.getInstance("EUR")  );
    MoneyObject myMoney2 = new MoneyObject( 0.5, Currency.getInstance("EUR")  );
    MoneyObject myMoney3 = new MoneyObject( 15, Currency.getInstance("USD")  );

    if ( myMoney.substract(myMoney2).getValue() != 9 )
        throw new Exception("testSubstract failed - Substract is wrong");

    try {
        MoneyObject tempObject = myMoney.substract(myMoney3); // Should fail

        throw new Exception("testSubstract failed - Currency check does not work!");
    } catch (Exception e){
        // Do nothing,
        // Exception is expected
    }
} 

/** 
* 
* Method: toString() 
* 
*/ 
public void testToString() throws Exception {
    MoneyObject myMoney = new MoneyObject( 9.5, Currency.getInstance("EUR")  );
    if ( !myMoney.toString().contains("9,5") )
        throw new Exception("testToString failed - bad formatting");
} 



public static Test suite() { 
return new TestSuite(MoneyObjectTest.class); 
} 
} 
