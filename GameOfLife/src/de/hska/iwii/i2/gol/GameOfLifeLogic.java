package de.hska.iwii.i2.gol;

/**
 * Die eigentliche Spielelogik. Das Spielfeld wird hier nicht
 * als zyklisch geschlossen betrachtet wird.
 *
 * @author Holger Vogelsang
 */
public class GameOfLifeLogic {

    /**
     * Current Generation cycle
     */
    boolean[][] currentGeneration;

    /**
     * Max X Coordinate
     */
    int maxX = 0;

    /**
     * Max Y Coordinate
     */
    int maxY = 0;

    /**
     * Takes the inital generation array
     *
     * @param generation Startgeneration
     */
    public void setStartGeneration(boolean[][] generation) {

        // Is generation valid ?
        assert generation != null;

        // Is generation dimension valid ?
        int xSize = generation.length;
        assert xSize > 0;

        int ySize = generation[0].length;
        assert ySize > 0;

        // Save data into attributes
        this.currentGeneration = generation;
        this.maxX = xSize - 1;
        this.maxY = ySize - 1;
    }

    /**
     * Calculates the next Generation
     */
    public void nextGeneration() {

        // Deep-work-copy of current generatio
        boolean[][] nextGeneration = new boolean[this.maxX + 1][this.maxY + 1];

        // Iterate over every cell ( row first, then col )
        // -> Calc each following status
        for (int x = 0; x <= this.maxX; x++)
            for (int y = 0; y <= this.maxY; y++)
                nextGeneration[x][y] = this.getNextCellStatus(x, y);

        this.currentGeneration = nextGeneration;
    }

    /**
     * @param x X Value (Column)
     * @param y Y Value (Row)
     * @return Status of Cell in next Cycle
     */
    protected boolean getNextCellStatus(int x, int y) {
        boolean currentStatus = this.currentGeneration[x][y];
        int amountOfLivingNeightbours = this.getAmountLivingNeighbours(x, y);

        if (!currentStatus) {
            if (amountOfLivingNeightbours == 3)
                return true; // Cell's magical resurrection
        } else {
            if (amountOfLivingNeightbours > 3 || amountOfLivingNeightbours < 2)
                return false; // Cell's death
        }

        return currentStatus; // Cell status does not change
    }

    /**
     * Calculates the Amount of Living Neighbours of a cell
     *
     * @param x x Coordiante
     * @param y X Coordinate
     * @return Amount of Living Neighbours
     */
    protected int getAmountLivingNeighbours(int x, int y) {

        // Setting up borders for "neighbours iteration"
        int minXCord = (x - 1 < 0 ? x : x - 1); // Min Value = 0
        int maxXCord = (x + 1 > this.maxX ? x : x + 1); // Max Value = Amount of Columns - 1
        int minYCord = (y - 1 < 0 ? y : y - 1);
        int maxYCord = (y + 1 > this.maxY ? y : y + 1);

        int amountLiving = 0;

        // Iterating over possible neighbours
        for (int curY = minYCord; curY <= maxYCord; curY++) {
            for (int curX = minXCord; curX <= maxXCord; curX++) {
                if (!(curY == y && curX == x)) {
                    if (this.currentGeneration[curX][curY])
                        amountLiving++;
                }
            }
        }

        return amountLiving;
    }

    /**
     * Checks if Cell is living
     *
     * @param x x Coordinate of Cell
     * @param y X Coordinate of Cell
     * @return Returns true if Cell is alive; False if dead
     */
    public boolean isCellAlive(int x, int y) {
        // Check if generation is null?
        assert this.currentGeneration != null;

        // Check if coordiantes are out of bounds ?
        assert x < this.maxX && x >= 0;
        assert y < this.maxY && y >= 0;

        // Give back my boolean to check for life
        return this.currentGeneration[x][y];
    }
}